# Quando

**27-28-29 giugno**

# Dove
**Spazio Sociale Autogestito [XM24](http://www.ecn.org/xm24)**  
via Fioravanti 24, Bologna

**XM24 è uno Spazio Pubblico Autogestito basato sull'antifascismo, antisessismo ed  antirazzismo. Fascist\*, xenofob\*, omofob\*, sessist\* non sono benvenut\* né il alcun modo tollerat\***

mappe [openstreetmap](http://osm.org/go/xdUSVJUuH?way=148997202)

<div class="small-6 medium-8">
	<iframe width="100%" height="300px" frameBorder="0" src="http://umap.openstreetmap.fr/it/map/xm24_7734?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&datalayersControl=false&displayCaptionOnLoad=false&displayDataBrowserOnLoad=false"></iframe><small><a href="http://umap.openstreetmap.fr/it/map/xm24_7734">See full screen</a></small>
</div>

## Come arrivare all'XM24

### Dall’aereoporto Marconi (BLQ)

#### Più economico:

Uscito dal terminal "Arrivi" dirigiti a sinistra, passa attraverso il 
parcheggio autobus e segui il percorso pedonale fino ad arrivare ad una grande
rotonda. Attraversa la strada, passa sotto il cavalcavia e dopo
100 metri sulla destra trovi la fermata “Birra” dove passa il bus [81/91](http://www.tper.it/content/linea-81-stazione-centrale-longara-padulle-bagno-di-piano) 
che ti lascia in stazione centrale. Prosegui seguendo le indicazioni qui sotto. 
Il biglietto urbano si può fare nel bar/tabacchino di fronte alla fermata. Fuck the BLQ.

#### Più costoso
Prendi il bus [BLQ](http://www.tper.it/content/linea-blq-aeroporto-stazione-centrale) 
di fronte all'aeroporto e scendi in stazione centrale. 
Prosegui seguendo le indicazioni qui sotto. 

### Dalla stazione centrale

#### A piedi/In bici
Esci dal retro della stazione ("Uscita via Carracci") attraversa la strada e 
gira a sinistra fino ad incontare sulla tua destra via Fioravanti (noterai 
un grosso edificio a vetri). Percorrila per circa 700 metri 
e sulla sinistra troverai l'ex-mercato dove sta XM24. 
Passa sotto la torretta ed entra nel parcheggio: alla tua destra c'è il cancello di XM24. 

Se arrivi nei giorni precedenti ad HackIT, invece di passare sotto la torretta prosegui 100 metri ed arriva alla rotonda: vicino alla parete con il murale di Blu c'è l'entrata principale. 

#### In autobus
Sei sicuro? Fai prima a piedi! Comunque, le linee 21, 27, 25, 35 passano tutte nei pressi dell'uscita principale della stazione (Medaglie d'oro, Autostazione, Indipendenza) e arrivano fino
Piazza dell’Unità, da li potete prosegui a piedi per circa 550 metri lungo
Via Tibaldi o via Bolognese fino ad arrivare di fronte all'ex-mercato ortofrutticolo. 
Passa sotto la torretta ed entra nel parcheggio: alla tua destra c'è il cancello di XM24. 
Se arrivi nei giorni precedenti ad HackIT, invece di passare sotto la torretta vai verso la rotonda: vicino alla parete con il murale di Blu c'è l'entrata principale. 
Se prendi l’autobus 11 puoi scendere alla seconda fermata di Via Bolognese, a circa 100 metri dalla rotonda.

Ulteriori informazioni sui servizi di linea:
[TPER](http://www.tper.it/sites/tper.it/files/TPER_Mappa_nov_13.pdf)

### In auto 
- Se arrivi da ovest esci dall'autostrada a Borgo Panigale e prendi la tangenziale in direzione S.Lazzaro. 
- Se arrivi da nord esci dall'autostrada a Bologna Arcoveggio e prendi la tangenziale in direzione Casalecchio. 
- Se arrivi da est/sud esci dall'autostrada a S.Lazzaro e prendi la tangenziale in direzione Casalecchio. 

In ogni caso esci dalla tangenziale all'uscita 6 zona Arcoveggio/Corticella. 
Percorri via Corticella andando verso il centro, per un paio di km. 
Arrivati al semaforo di Piazza dell’Unità per le auto la svolta è obbligata a destra in Via Bolognese. Percorrila tutta arrivando alla rotonda e 
sulla sinistra puoi vedere XM24. Trova parcheggio (dentro ad XM NON c'è posto). 


# Dormire
Nel piazzale posteriore di XM24 sarà possibile campeggiare. E' al sole ma stiamo approntando una parziale copertura e portatevi un materassino perchè potrebbe capitarvi di dormire sulla parte in cemento. 
Lo spazio non è moltissimo ma dovrebbe bastare per le tende (please, limitate camper e furgoni). 
All'interno saranno a disposizione la palestra e, come da tradizione, ogni luogo non dedicato alla computazione. 

# Ingresso
Come ogni anno, l'ingresso all'hackmeeting e' del tutto libero: ricordati
pero' che organizzare l'hackmeeting ha un costo, e le sottoscrizioni sono
necessarie per la sopravvivenza di questo evento!

Se hai intenzione di venire prima di venerdi', **ottimo!**. Ti chiediamo
pero' di **comunicarcelo** in modo da poterci organizzare.

# Cosa devo portare
Se hai intenzione di utilizzare un computer portalo accompagnato da una
ciabatta elettrica. Non dimenticare una periferica di rete di qualche tipo
(vedi cavi ethernet e/o dispositivi wifi).

Durante l'evento **non** è garantita connessione internet, dunque se pensi
di averne bisogno portati una pennina 3G e il necessario per condividerla con
gli amici! In generale, cerca di essere autosufficiente sul lato tecnologico.

# Cibo

Durante l'evento verranno organizzati i pasti, dunque non c'è bisogno di
organizzarsi individualmente (se però sei un "vero campeggiatore"&trade; portati piatto/forchetta/coltello).

*Il menu sarà vegetariano o vegano. Se hai particolari necessità alimentari comunicacele.*
L'XM24 è comunque in zona centrale, nei dintorni ci sono molte altre soluzioni.

# Organizzazione
Hackmeeting è un evento completamente autogestito: tutte le attività necessarie
(cucina, pulizie, bar, registrazione seminari, ingresso, etcetc) per un buon svolgimento sono su base volontaria. Durante i 3 giorni i turni saranno organizzati attraverso tabelloni collocati in punti strategici: segnati anche tu e **fai** Hackmeeting! 


# Ancora dubbi?

[Contattaci](http://it.hackmeeting.org/contatti/)

