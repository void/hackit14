---
title: "Wikimedia: condivisione, comunità, conflitto"
date: 2014-06-27T22:00:00
room: decimale
slide:
img:
externalurl: 
who: "atropine"
---

Come funziona l'enciclopedia collaborativa più famosa del mondo? Con quali meccanismi si regola la sua comunità, e da chi è composta? Quali sono le sfide che Wikipedia si trova oggi ad affrontare? Cercheremo di rispondere a queste domande (e a tutte quelle che vi verranno in mente) per provare a stabilire un ponte con la comunità hacker e stimolare il dibattito su problematiche e criticità comuni.
