---
title: "Alla ricerca della black-op perduta"
date: 2014-06-28T00:30:00
room: decimale
slide:
img:
externalurl: 
who: "Gilda35: Trimetil, Norrin, Cippalippa e Cipolla"
---

Quale futuro per la #BotnetDemocracy?

Dall'Esperimento del #Katzing alla #BotnetDemocracy.  
Graffiti digitali dadaisti, conditi con tecniche non convenzionali, per scherzare con gli sciami e con il professionismo sui bigdata.  
Se la teoria degli sciami è semplicemente il parto di romantici fricchettoni della rete, come chiamare quella largamente utilizzata dalle 'Organizzazioni'?  
Abbiamo scherzato PRISM e i suoi amichi, oppure stato tutto frutto dell'immaginazione? E soprattutto, perché erano tutti così interessati al Katzing Project ed alle successive alterazioni?  
Sarà ancora possibile giocare con i Tecno-nucleo, emergere dal rumore, oppure tutto è perduto? E come stanno cambiando per resistere agli assalti convenzionali e non convenzionali?  
Racconti dalle retroguardie di Gilda35 mentre l'avventura continua...

----
Hackmeeting 2014: Addio e grazie per tutto il Katzing  
http://gilda35.com/2014/07/hackmeeting-2014-addio-e-grazie-per-tutto-il-katzing.html
