---
title: "Non di solo 2.4 vive l'hacker (SDR,GSM)"
date: 2014-06-27T15:00:00
room: binario
slide:
img:
externalurl:
who: "blackflag"
---

Grazie all'entrata in commercio delle chiavette rtl-sdr a bassisimo costo (~12
euro), si e' aperta una nuova strada del radio hacking. Pochi software e molto
grezzi stanno tentando di assaltare il GSM e i protocolli radio
digitali. Vediamo assieme lo stato dell'arte e i possibili sviluppi...
