---
title: "Programmare la creatività"
date: 2014-06-28T16:00:00
room: binario
slide:
img:
externalurl:
who: "kundera"
---

La creatività è un'abilità che si apprende e si può sviluppare. Il Pensiero Creativo può aiutare a trovare soluzioni non convenzionali ai problemi che si presentano quotidianamente. L'intervento sarà focalizzato sullo sviluppo del pensiero creativo nell'ambito dello sviluppo software. In particolare si analizzeranno i sette step propri del metodo creativo e si sveleranno le principali tecniche utilizzate per esprimere le potenzialità inespresse ;)P La presentazione, realizzata per il al Codemotion Roma 2014, sarà ricalibrata per l'hackmeeting!

http://www.slideshare.net/lusiola/programmare-la-creativit-codemotion-roma-2014