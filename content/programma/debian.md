---
title: "Debian: a geeky quasi-anarchy that works"
date: 2014-06-28T16:00:00
room: decimale
slide:
img:
externalurl:
who: "Stefano Zack Zacchiroli"
---

Presentation of the Debian project, focusing on community and self-organization
aspects.
