---
title: "Via del Mare Racconta"
date: 2014-06-27T19:00:00
room: decimale
slide:
img:
externalurl:
who: "Mara Cinquepalmi"
---

Un progetto tra data journalism e memoria in un'ottica di genere.
dedicato alle lavoratrici dello stabilimento di Foggia dell’Istituto Poligrafico
e Zecca dello Stato.

http://www.viadelmareracconta.it/
