---
title: "Openstreetmap"
date: 2014-06-27T16:00:00
room: esadecimale
slide:
img:
externalurl:
who: "Simone Cortesi"
---


http://openstreetmap.org

Come si è arrivati a costruire una mappa collaborativa, libera da vincoli di
riuso, ed ispirata a Wikipedia.

Differenze con le mappe commercialmente disponibili e
come le persone siano in grado di partecipare aggiungendo dettagli.
