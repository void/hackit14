---
title: "Jaromail"
date: 2014-06-27T17:00:00
room: esadecimale
slide:
img:
externalurl:
who: "jaromil + alv"
---

Caratteristiche e funzionamento di un applicazione da terminale di facile istallazione (GNU/Linux o Apple/OSX) basata su Mutt, GnuPG, Procmail, Fetchmail, Mairix, Msmtp, Abook, Sieve e Zsh
