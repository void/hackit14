---
title: "ArLoDe"
date: 2014-06-27T01:00:00
room: binario
slide:
img:
externalurl: "http://www.youtube.com/watch?v=Fr28xjw5ves"
who:
---

ArLoDe è la crasi che descrive un quanto di tempo tra ARte LOgica e puro DElirio, nella ricorsione caotica dell'informazione che ci attraversa.

Trasformeremo una telecamera ed una televisione, entrambi analogiche, in un sistema informatico digitale tramite metodi analogici, creando una macchina in grado di fare delle computazioni informatiche. Atraverso gli automi cellulari (Game of Life), la teoria dell'informazione e la teoria del caos, computeremo l'informazione-automatica stessa hackerando l'informazione dal punto di vista concettuale, generando e computando nuove informazioni per migliorare il contesto che ci circonda.

Il Talk sarà diviso in 2 parti: Una spiega come funziona il sistema del “AutoVaneggiatore Elettrico”, l'altra è ArLoDe che è un talk in mezzo tra una performance artistica ed un talk vero e prorio sull'informazione dal punto di vista concettuale.
