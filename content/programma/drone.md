---
title: "Drone/Quadricottero DIY"
date: 2014-06-28T14:00:00
room: esadecimale
slide:
img:
externalurl: 
who: "dead3t3rn1ty"
---

 I Droni cominciano a ricevere attenzione mediatica. In questo talk analizzerò la struttura e il funzionamento, dalla meccanica di base alla costruzione e programmazione di un drone da zero, con una stampante 3D, 4 motori brushless coi rispettivi speed controller, un controller arm generico, 3 accellerometri, un giroscopio, una batteria LiPo e un radiocomando.
