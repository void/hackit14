---
title: "Pimp My Vim"
date: 2014-06-27T22:00:00
room: binario
slide:
img:
externalurl: 
who: "Ricciocri"
---

Vim ha a disposizone un numero estremamente alto di plugin che lo rendono per alcuni task equivalente (se non superiore) ad IDE molto più blasonati come Eclipse. In questo talk mostrerò alcuni dei plugin che uso giornalmente.
