---
title: "Openaccess vs. Scienza Chiusa"
date: 2014-06-28T12:00:00
room: esadecimale
slide:
img:
externalurl: 
who: "horn + piero_tasso"
---

Come funziona l'editoria Open Access e perché se ne sente il bisogno? Sarebbe interessante conoscere il mondo chiuso dell'editoria scientifica: quali diritti ha l'autore di una ricerca? quali diritti hanno studenti e ricercatori? avete mai letto una licenza di un journal scientifico chiuso?
