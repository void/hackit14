---
title: "Rete delle Biblioteche Anarchiche e Libertarie"
date: 2014-06-28T17:00:00
room: esadecimale
slide:
img:
externalurl:
who: "Jops & Tubia"
---

Il giorno 28 giugno durante l'hackmeeting si terrà la presentazione del progetto RebAl, ovvero una rete di collaborazione tra biblioteche, archivi e centri di documentazione specializzati in storia, teorie e culture dei movimenti anarchici e libertari.
Il progetto RebAl parte dall’iniziativa di un gruppo di archivi e biblioteche italiani, ma intende proporsi come punto di riferimento a livello internazionale per la collaborazione operativa tra centri aderenti alla rete FICEDL (Federazione internazionale dei centri studi e di documentazione libertari).
Per la prima volta verrà presentato il portale <A HREF="http://www.rebal.info" TARGET="_blank">rebal.info</a>, ovvero un motore di ricerca in grado di offrire un punto di accesso unificato alle risorse delle varie biblioteche e archivi aderenti.
Realizzato attraverso software libero il motore di ricerca è capace di integrare non solo cataloghi di biblioteche ma anche ulteriori risorse come biblioteche digitali, inventari archivistici, repertori bibliografici e riviste ad accesso aperto.
Il principio ispiratore di rebAl è la volontà di facilitare l’accesso pubblico al patrimonio culturale anarchico, nella convinzione che la sua più ampia circolazione sia uno strumento importante nei processi di trasformazione sociale e di diffusione dei principi e delle pratiche antiautoritarie.
Parteciperanno alla presentazione alcune delle biblioteche aderenti al progetto e gli attivisti che hanno realizzato il portale.
