---
title: "Hacking (with) your brain"
date: 2014-06-27T19:00:00
room: binario
slide:
img:
externalurl: 
who: "benry"
---

In questo talk si parlera' delle BCI (Brain-Computer Interfaces): sistemi in grado di acquisire segnali modulati dall'attivita cerebrale di un utente e di tradurli in comandi per un computer od altri devices in genere.

A seguito di una panoramica generale e di una discussione sullo stato dell'arte, ci si concentrerà su un particolare tipo di BCI, basato sulla lettura di segnali elettroencefalografici acquisiti con elettrodi non invasivi. In particolare, su un metodo basato sull'identificazione di risposte SSVEP (Steady-State Visually Evoked Potentials).

Si parlera' principalmente di questo tipo di BCI in quanto e' uno dei piu' semplici da implementare, ed anche uno tra i piu' efficaci. Contestualmente, per incentivare sperimentazioni da parte dei partecipanti, verranno anche introdotti:

- una schedina per l'acquisizione dei segnali elettroencefalografici open hardware che dovrebbe essere rilasciato a breve, [OpenBCI](http://www.openbci.com/).

- il software libero [OpenVibe](http://openvibe.inria.fr/).
