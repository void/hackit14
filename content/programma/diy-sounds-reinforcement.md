---
title: "D.I.Y Sound Reinforcement"
date: 2014-06-28T00:30:00
room: binario
slide:
img:
externalurl: 
who: "Candini (ImpaktSound)"
---

Concetti base per l'autocostruzione di un sound system

Infarinata generale sul suono, la sua percezione, la sua composizione, per poi passare a definire che cosa c'è e cosa ci dovrebbe essere in un sound system con descrizioni di processori, amplificatori, mixer e quant'altro e per finire una guida su come prendere in mano il legno, quale legno scegliere, lavorazioni, strumenti ed errori comuni.
