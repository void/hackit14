---
title: "Internet e' rotta: colonialismo digitale"
date: 2014-06-28T18:00:00
room: decimale
slide:
img:
externalurl: 
who: "iliasbartolini"
---

 Bella la favola di una Internet libera e neutrale: ma chi continua a produrre i contenuti, chi li pubblica e quali speranze ci sono per non ripetere la storia dell'oppressione imperialista delle culure dominanti? Vediamo alcune informazioni introduttive e poi discutiamone insieme.
