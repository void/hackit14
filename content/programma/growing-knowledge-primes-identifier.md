---
title: "Growing Knowledge Primes Identifier"
date: 2014-06-28T23:59:00
room: esadecimale
slide:
img:
externalurl: 
who: "$witch"
---

**SE** l' insieme dei naturali N puo' essere diviso in 2 sottoinsiemi mutuamente esclusivi, i primi (P) ed i compositi (Q) **ALLORA** 1 e' primo, alla faccia di wikipedia e chiunque altro

**SE** 1 appartiene ai P E l'operazione di somma esiste **ALLORA** l'unica regola SEMPRE valida per generare un candidato-primo ( C ) e' di provare TUTTI gli N

**SE** poi 2 appartiene ai P **ALLORA** in un dato sottoinsieme di N esiste una regola che contiene maggiore informazione per generare i C : 2N+1

**SE** infine e' possibile definire intervalli e regole opportune **ALLORA** e' anche possibile ingegnerizzare un software che accumuli conoscenza e divenga sempre piu' efficiente.

poiche' «talk is cheap, show me the code» è applicabile al caso: 
dimostrazione pratica di come implementare l'algoritmo descritto.