---
title: "#migrantsfiles"
date: 2014-06-27T18:00:00
room: decimale
slide:
img:
externalurl: 
who: "dataninja / andrea nelson mauro"
---

Dal 2000 al 2013 sono morti 23000 migranti nel tentativo di raggiungere l'Europa. E' il risultato principale di MigrantsFiles, un'inchiesta internazionale di data journalism realizzata da più di 10 giornalisti in 6 Paesi europei, che ha raccolto tutte le fonti open sources esistenti e mostrato che le politiche europee sul controllo delle rotte aggravano il bilancio. Tra questi dati, anche quelli raccolti da Gabriele Del Grande, autore del blog Fortress Europe.

* http://vimeo.com/97210043
* http://stories.dataninja.it/themigrantsfiles
* http://www.themigrantsfiles.com
