---
title: "Bitcoin: Introduzione al mondo delle crittovalute"
date: 2014-06-28T12:00:00
room: binario
slide:
img:
externalurl: 
who: "HostFat"
---

 Il talk sarà una presentazione basilare di che cos'è il Bitcoin, partendo dalla base, e cioè come nasce una moneta. L'obbiettivo è di scaturire l'interesse dei presenti allo studio e alla scoperta delle opportunità che questa nuova tecnologia offrirà.
