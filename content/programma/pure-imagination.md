---
title: "Pure Imagination"
date: 2014-06-27T00:30:00
room: binario
slide:
img:
externalurl: 
who: "Video Animazioni Vive"
---

2011-2013. Report di due anni di performance con PD. Ovvero, l'amore e l'odio che suscita un terminale nei confronti di un approccio analogico.

..siamo nel cosiddetto ambito delle 'nuove tecnologie', taggabili come 'electronic music' e 'visual art', ma di fatto, quando uno libera la propria creatività non si limita ad usare solo le dita e gli occhi.. Si attivano tutti i sensi, e con un'estrema esigenza di manifestarsi.

..ma i nostri sensi, che ne sanno di tecnologia?

http://www.d-e-n-s-o.org/pure
