---
title: "Google Tips & Tricks + The Dork side of Google"
date: 2014-06-27T17:00:00
room: binario
slide:
img:
externalurl:
who: "epto + b3rito + Mes3HackLab"
---

Google Tips & Tricks

Spiegazione di che cosa sono e come si sfruttano i google dorks. Come gli utenti diversamente hacker possono usare i google dorks per salvarsi la serata.


The Dork side of Google

Questo talk spiega in maniera semplice come ottenere le shell sui siti governativi infettati. Naturalmente è solo il caso di mostrarle dalla chache di Google, eseguirle sarebbe tutt'altra cosa.

Grazie ad una svista di molti “attaccanti” è possibile scoprire con delle mirabolanti peripezie dei google dorks, tutti i siti che sono vulnerabili ed infettati con le shell più diffuse.

Sarà diffuso un programma a sorpresa
