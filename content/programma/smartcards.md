---
title: "Programming SmartCards for fun and..."
date: 2014-06-28T18:00:00
room: esadecimale
slide:
img:
externalurl: 
who: "Gipoco"
---

Seminario teorico/pratico: prima un'introduzione teorica, successivamente un
esempio di programmazione, quindi l'analisi di due scenari di attacco
ampliamente illustrati, infine (dopo il seminario) eventuale capanna dei suki
con gli interessati per fare qualche prova con vari amenicoli che
condividerò.
