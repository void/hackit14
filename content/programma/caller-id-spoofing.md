---
title: "Caller ID Spoofing e violazione della privacy"
date: 2014-06-28T16:00:00
room: esadecimale
slide:
img:
externalurl: 
who: "jabex"
---

Da alcuni anni negli USA e in alcuni paesi europei si parla di intercettazioni della voicemail. In questo talk analizzerò lo stato d'arte delle tecniche di intercettazione della voicemail tramite una proof of concept. Verrà preso in considerazione in particolare un operatore di telefonia mobile italiano.
