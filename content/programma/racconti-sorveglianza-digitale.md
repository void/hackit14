---
title: "Racconti di sorveglianza digitale"
date: 2014-06-28T22:00:00
room: decimale
slide:
img:
externalurl: 
who: "Claudio Nex Guarneri"
---

Nel 2011 l'avvento della Primavera Araba e il rovesciamento di diversi regimi autoritari ha portato alla luce importanti informazioni su un oscuro mercato precedentemente sconosciuto ai piu'. Si e' scoperta e documentata la complicita' tra diverse societa' Europee, incluse Italiane, e governi e regimi di tutto il mondo per la fornitura di tecnologie di sorveglianza di intrusione spesso utilizzate per spiare dissidenti, giornalisti ed altre personalita' scomode. 
Il 2013 ha scosso il mondo con le rivelazioni sulle attivita' di NSA e GCHQ ed ha portato il tema di privacy e sorveglianza all'attenzione del pubblico e di organi politici internazionali.Le fazioni governative e commerciali parti del business della sorveglianza sono ora sotto attento scrutinio tecnico, sociale e legislativo.
In questa presentazione ripercorriamo questi eventi, i dettagli tecnici ed i retroscena vissuti in prima linea.
