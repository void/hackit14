---
title: "Adunanza per la difesa tecnologica"
date: 2014-06-28T14:00:00
room: binario
slide:
img:
externalurl: 
who: "ihp"
---

La grave minaccia al Web aperto da parte piattaforme Internet proprietarie, la pervasivitá dell’abuso della privacy emerse con Snowden e le nuove tecnologie di controllo globale presentate come metodi per la sicurezza. Sono queste le cose che ci spingono al cambiamento:

Sempre di più i politici e le amministrazioni mettono in circolo nuovi sistemi di sorveglianza e tecnologia che producono dati. In questa loro situazione di monopolio riescono a convincere gli ignari elettori a consegnare la propria libertà a società o peggio a realtà come NSA.

Dobbiamo renderci conto che, oggi, c'è un'esigenza di creare un *gruppo di intelligence* controllato dalla scena underground che sia in grado di rispondere alle continue minacce sul nascere.

Questo non è solo un talk, ma una chiamata per l'Adunanza di persone che intendono attivarsi per un progetto di liberazione dalle tecno-dittature.


Esempi su cui si opera

* Droni Attivarsi per renderli inefficenti, disattivarli, ecc…
* RFID Contrastare gli usi lesivi delle tecnologie RFID.
* Rete Produrre un sistema di comunicazione crittato e divulgare dati fittizzi verso NSA e le altre minacce globali.
* Altro Verificare se le nuove tecnologie possono essere lesive della privacy o se possono essere un sistema di controllo e disinnescarle.


Modalità in cui si agisce

* Legale Finchè possibile muoversi in modo lecito ed aperto.
* Anonimo Uitilizzazione dei sistemi per la privacy e l'anonimato.
* Tecnologico Creare sempre l'antidoto alle minacce.
* Informativo Diventare un punto di riferimento attendibile per la valutazione dei rischi.
* Attivo Agire attivamente contro le tecnologie più minacciose.


La peggior cosa che si può fare per non cambiare le cose in meglio è la “delega”.Ora occupiamoci di ciò che è nostro.
