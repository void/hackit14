---
title: "Crypto genealogia"
date: 2014-06-27T19:00:00
room: esadecimale
slide:
img:
externalurl:
who: "Collettivo Ippolita"
---

Metodi di genealogia tecnica: il caso della crittografia

perché la genealogia? parole affini e/o della stessa famiglia: genetica; genitori; generare; generazione; gente… Tracciare la genealogia di una tecnica che si utilizza, magari che si vorrebbe diffondere, significa quindi capire da dove si viene, abbozzare la propria storia.

Vorremmo sottoporre parecchie espressioni correnti e parole a questo metodo, parole che hanno subito o stanno subendo forme di esproprio cognitivo, per cui non sono più nostre, ma appannaggio di qualcuno che ci lucra sopra (vedi alla voce “open source”, “decentralizzazione”, ecc.). Il quadro più ampio sono le differenti genealogie del concetto di libertà e le sue implementazioni tecniche, a partire dal movimento cypherpunk (anni '90) fino a Wikileaks e oltre. La crittografia è ancora oggi il concetto tecnico chiave teso a promuovere e difendere le libertà: questo lo rende un buon punto di partenza.

Chiunque puo' usare il metodo genealogico: useremo solo archivi (Foucault 1969), ma nulla di segreto: solo documenti pubblicati e ampiamente disponibili sul web. Non abbiamo nessuna informazione “da dentro”, nessun leak, nessuna informazione top secret da spifferare. Siamo pero' parte del metodo genealogico perché facciamo parte di questa storia, non siamo estranei alla crittografia, siamo implicati nella costruzione dei mondi digitali da ben prima che apparisse il Web. è una storia degli eventi, delle discontinuità, di quei piccoli cambiamenti tecnici che accompagnano l'emergere di un'idea, di una pratica. e delle relazioni di potere che queste emersioni implicano. L'idea della genealogia tecnica è quindi molto semplice:

delimitare un insieme di dati e agire come filtri umani per ricostruire un resoconto affidabile in un tempo ragionevole. Un resoconto di “come sono andate le cose secondo noi”, e non una verità oggettiva servita da qualche algoritmo che non controlliamo ma che alimentiamo affidandoci ai filtri dei padroni digitali.


Bibliografia
------------

- Akrich, Madeleine (2006). [La description des objets techniques](http://books.openedition.org/pressesmines/1197), pp. 159 – 178.
- Anspach, Mark (2013). [Julian Assange’s Double Trouble](http://www.imitatio.org/mimetic-theory/mark-anspach/julian-assanges-double-trouble.html).
- Assange, Julian et al. (2012). Cypherpunks: Freedom and the Future of the Internet. OR Books, LLC.
- Bell, Jim (2013). [Brief explanation of anarcho capitalism](http://jim.com/anarchy/)
- Callon, Michel (2006). [Pour une sociologie des controverses technologiques](http://books.openedition.org/pressesmines/1196), pp. 135 – 157.
- Coleman, Gabriella E. (2012). [Coding Freedom: the Ethics and Aesthetics of Hacking. Princeton: Princeton University Press](http://codingfreedom.com/buy_download.html).
- Davis, Erik (2004). Techgnosis: Myth, Magic and Mysticism in the Age of Information. London: Serpent’s Tail.
- Dreyfus, Suelee and Julian Assange (2012). Underground: Tales of Hacking, Madness and Ob- session on the Electronic Frontier. Edinburgh: Canongate Books. http://www.underground-book.net/download.php3
- Foucault, Michel (1969). L’archéologie du savoir. Paris: Gallimard.
- Freeden, Michael (1996). Ideologies and Political Theory: A Conceptual Approach. Oxford: Ox- ford University Press.
- Matthes & Seitz Berlin (2012). Transparenzgesellschaft.
- Hughes, Eric (1993). [A Cypherpunk’s Manifesto](http://www.activism.net/cypherpunk/manifesto.html).
- Ippolita (2012). [Nell’acquario di Facebook. La resistibile ascesa dell’anarco-capitalismo](http://www.ippolita.net/it/libro/nellacquario-di-facebook-online)
- Lanier, Jaron (2010). [The Hazards of Nerd Supremacy: the Case of WikiLeaks](http://www.theatlantic.com/technology/archive/2010/12/the-hazards-of- nerd-supremacy-the-case-of-wikileaks/68217/). In: The Atlantic.
- Latour, Bruno (1993). We Have Never Been Modern. Trans. by Catherine Porter. Cambridge: Harvard University Press.
- Lovink, Geert and Patrice Riemens (2013). [Twelve thesis on Wikileaks ](http://www.eurozine.com/articles/2010-12-07-lovinkriemens-en.html). In: Beyond WikiLeaks: Implications for the Future of Communications, Journalism and Society. Ed. by Benedea Brevini, Arne Hintz, and Patrick McCurdy. London: Palgrave Macmillan.
- May, Timothy C. (1988). [The Crypto Anarchist Manifesto](http://www.activism.net/cypherpunk/crypto-anarchy.html). — (1994a). [Release Notes for Cyphernomicon](http://groups.csail.mit.edu/mac/classes/6.805/articles/crypto/cypherpunks/cyphernomicon/Release-Notes) — (1994b). [Cyphernomicon: Cypherpunks FAQ and More](https://cypherpunks.to/faq/cyphernomicron/cyphernomicon.html).
- Stephenson, (1992) [Neal Town Snow Crash](http://hell.pl/agnus/anglistyka/2211/Neal%20Stephenson%20-%20Snow%20Crash.pdf)
- Stephenson, (1999) [Cryptonomicon](http://www.tearsoft.com/Cryptonomicon.pdf)
- Wigenstein, Ludwig (2009). Philosophical Investigations. Trans. by Gertrude Elizabeth Margaret Anscombe, Peter Michael Stephan Hacker, and Joachim Schulte. Oxford: Wiley & Blackwell.
- Zimmermann, Philip (1990). [Pretty Good Privacy, RSA Public Key Cryptography for the Masses: PGP User’s Guide]( http://openpgp.vie-privee.org/manupgp1.htm).
