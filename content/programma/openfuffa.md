---
title: "Openfuffa, la carta igienica e Atom Editor"
date: 2014-06-28T22:00:00
room: binario
slide:
img:
externalurl: 
who: "grigio"
---
 Chi siamo noi? Da dove veniamo? Perché Trenitalia™ salva ancora le password in chiaro? Perché su internet si diffonde più stupidità che conoscenza? Un viaggio surreale interattivo informale tra codice, web realtime con MeteorJS, obsolescenza programmata e opendata italiani.

Verrà presentato il progetto #TRENZI (Social Media Monitor sul Governo di Renzi con Smart Buzz Sentiment Analisys™) e altri esperimenti codice + data divertenti. 
