---
title: "Linux cli for dummies"
date: 2014-06-28T10:00:00
room: binario
slide:
img:
externalurl: 
who: "panda"
---

Prima o poi nella vostra convivenza forzata con linux, avrete un prolema che richiedera' di aprire un terminale. Se il termine shell vi fa pensare al deodorante, se la parola terminale vi ricorda gli aeroporti, e i caratteri bianchi su sfondo nero vi fanno sudare freddo, questo seminario e' per voi​
