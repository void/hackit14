---
title: "Progetto Anopticon"
date: 2014-06-27T16:00:00
room: binario
slide:
img:
externalurl:
who: epto
---

Anopticon è un progetto di schedatura delle telecamere di videosorveglianza negli spazi pubblici.

Il progetto, attraverso la collaborazione dei propri utenti, racoglie la mappatura e la schedatura delle telecamere di videosorveglianza negli spazi pubblici misurando l'area videosorvegliata della città e di segnalare alle autorità competenti le violazioni della privacy e le telecamere illegali o non segnalate secondo la legge.

Sono state mappate le telecamere delle città di Venezia, Padova, Foggia, Urbino, Solero (provincia di Alessandria), Alessandria, Pisa, Genova, Mestrino, Cassano d'Adda, Bari, Verona, Castelprio, Marghera, Mestre (VE), Murano (VE), Pinerolo, Palmi, Mantova, Bari Loseto, vico equense e Roma ed ne è stata pubblicata in rete la mappa interattiva ([Big Brother viewer](http://tramaci.org/anopticon)).

La presentazione tratta anche argomenti sulle strategie di controllo mentale. 
