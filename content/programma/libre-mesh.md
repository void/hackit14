---
title: "Libre Mesh"
date: 2014-06-28T18:00:00
room: binario
slide:
img:
externalurl: 
who: "g10h4ck"
---

Libre-Mesh is an initiative undertaken by community networks members of several continents that collaborate towards a common goal: to develop a set of tools that facilitate the deployment of Free Networks on any community in the world.
