---
title: "Tails"
date: 2014-06-28T12:00:00
room: decimale
slide:
img:
externalurl:
who: "intrigeri"
---

intrigeri is a Debian and Tails contributor. He founded the Tails project in 2009.

In this talk, intrigeri will briefly present the goals, means and history of the Tails project. But don't be mistaken: this will primarily be a pretext to discuss two key aspects of Tails, and more generally of (free, security) software development, that he finds particularly important. Namely:

usability is a critical security feature;
“upstream first!” is the only way to survive on the OS development scene.
Then, he will go through the challenges faced by Tails for the next few years, and suggest many ways in which one can contribute to Tails, with very diverse skills.
