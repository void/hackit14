---
title: "Baobáxia nella Rotta dei Baobab"
date: 2014-06-27T18:00:00
room: esadecimale
slide:
img:
externalurl:
who: "NPDD, Nucleo di Ricerca e Sviluppo Digitale - Casa de Cultura Tainã / Rede Mocambos"
---

É necessario tempo, un altro ritmo, per ristabilire equilibri organici e sani
con gli strumenti tecnologici, per uscire dalle forme sfruttatrici dove la
tecnologia si sostituisce all'essere umano invece di cooperare.

La lotta per la terra ispira la ricerca per una terra digitale libera, dove per
terra intendiamo infrastruttura fisica e logica delle reti. Questa ricerca può
riprodurre il sistema colonizzatore e schiavista che vede migliaia di server
lavorando per pochi padroni, sempre, 24h/24 online, in grandi datacenter, le
“senzalas”, o può riflettere una relazione più umana e diretta con la terra che
quando rispettata ci alimenta, mentalmente e fisicamente.. 'ra bella!

Invece di installare server quindi coltiviamo mucuas, che sono frutti di
baobab. Baobab che molte comunità quilombola e mocambos hanno accolto como spazi
simbolici di memoria colletiva, identità e
[ancestralità](http://galeria.mocambos.net/tag/7/Rota+dos+Baob%C3%A1s).

E le mucue ospiterranno le memorie digitali locali con la possibilità di scambiarle con le mucue di altre comunità che compongono questa galassia di baobab, [Baobáxia](http://wiki.mocambos.net/wiki/NPDD/Baobáxia).

Aspettiamo l'arrivo di altri
[Nartigiani](http://mutgamb.org/fonte/Manifesto-nartisan), come direbbe mano
dpadua: “Smuovi quel culo ciccione e crea le tue reti. Trova gli strumenti
alternativi e crea tattiche per quello che ti serve. Si un camaleone
tecnologico. Un cyborg mosso dalla propria poesia.”
