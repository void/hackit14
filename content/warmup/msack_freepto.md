---
title: "Freepto: cryptousb 4 activists"
where: "Mensa Occupata"
city: Napoli
date: 2014-06-21
hour: "18:00"
flyer: "msack_freepto.jpg"
description: "Il MSAck Hacklab ritorna alla Mensa Occupata con una bella giornata antirepressiva."
passato: true
---


Hackit14 si avvicina...
Il MSAck Hacklab ritorna alla Mensa Occupata con una bella giornata antirepressiva.
Via Mezzocannone 14

alle 18:00
Presentazione di

*FREEPTO*
crypto-usb 4 hacktivists

*CRYPT OR DIE*
manualetto di autodifesa dalla repressione digitale

alle 21.00
*CENA DI AUTOFINANZIAMENTO*
+ dj set ElectroSwing

http://msack.org/2014/06/06/sabato-21-giugno-freepto-e-crypt-or-die/

Freepto è un sistema operativo installato su una pennetta usb.
L'idea che sta alla base dello sviluppo di Freepto è quella di offrire un sistema operativo semplice che permetta la gestione sicura degli strumenti utilizzati più di frequente dagli attivisti, senza però rinunciare alla comodità di un sistema operativo tradizionale.Posto che abbandonare l'utilizzo di sistemi operativi proprietari (Windows e Mac OSX) è il primo passo necessario per aumentare la nostra sicurezza, ci sono moltissimi casi in cui abbandonare completamente l'utilizzo di questi sistemi proprietari diventa difficile (magari per necessità lavorative), ed è per questo motivo che diventa importante trovare un modo pratico e veloce per separare l'account utilizzato a lavoro dall'account utilizzato per fare attivismo.
Verrà inoltre presentato l'opuscolo "Crypt or Die", il manualetto di autodifesa dalla repressione digitale,
Presentazioni a cura dell'AvANa hacklab.

msack.org
freepto.mx
hackmeeting.org
avana.forteprenestino.net 
