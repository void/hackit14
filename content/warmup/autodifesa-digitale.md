---
title: "Guida all’autodifesa digitale"
where: "eXploit, largo brunopontecorvo 3"
city: Pisa
date: 2014-05-31
hour: "10:00"
flyer: "autodifesadigitale.jpg"
description: "Incontri, dibattiti e workshop sui metodi crittografici utili a tutelare la nostra riservatezza su Internet."
passato: true
---


https://eigenlab.org/2014/05/guida-allautodifesa-digitale/

Una giornata volta a introdurre gli strumenti che permettono
comunicazione e scambio di dati al riparo da occhi indiscreti. Se le
rivelazioni su PRISM hanno reso noto che tutti gli utenti di internet
sono soggetti a controllo, le multinazionali che offrono servizi web
raccolgono e catalogano le informazioni personali dei propri utenti per
venderle al miglior offerente, e le polizie di tutto il mondo utilizzano
sempre di più l'acquisizione di prove informatiche nel corso delle
indagini per reprimere gli attivisti dei movimenti sociali, ci sono
metodi di autotutela basati su strumenti crittografici, che non sono
appannaggio di una ristretta cerchia di maghi del computer ma sono
utilizzabili da chiunque.

Programma:  
*MATTINO*  

 * Autistici/Inventati: Internet usato per il controllo su vasta scala: cosa ci dicono le rivelazioni di Snowden. Qualche parola sull'esperienza del collettivo Autistici/Inventati che da anni si occupa di tutela di riservatezza e liberta' in rete.
 * Igor Falcomatà (sikurezza.org): Problemi e soluzioni per la riservatezza e la sicurezza degli attivisti in rete. Quali sono le tecniche di attacco piu` utilizzate e come fare per difendersi?
 * AvANa presenterà Crypt'r'Die: una panoramica sugli strumenti legali e
tecnici della repressione digitale, su come prevenirla e come affrontarla

*PRANZO BUFFET*

*POMERIGGIO*  
Workshop pratico per imparare ad usare gli strumenti e i software legati alla sicurezza, come la distribuzione linux Freepto (sviluppata da AvANa), pgp, cifratura del disco, otr, chatsecure e retroshare.

Si consiglia di portare il proprio pc e una pennina usb!

sabato 31 maggio dalle ore 10 a exploit ([largo brunopontecorvo 3](http://www.openstreetmap.org/node/2463332403), davanti al dip. di matematica, Pisa)
