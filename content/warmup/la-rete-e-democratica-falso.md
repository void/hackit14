---
title: "La rete è libera e democratica. Falso"
where: "MODO Infoshop - via Mascarella"
city: Bologna
date: 2014-06-12
hour: "19:00"
flyer: ippolita.jpg
description: "Presentazione dell'ultimo libro del collettivo ippolita.net: decostruire le mitologie della rete."
passato: true
---

Si dice che Internet consente la libera circolazione di contenuti e idee, fruibili gratuitamente online, contro lo strapotere di cartelli mediatici e obsoleti detentori di copyright. Si racconta che sul Web può
nascere una nuova società civile, perché è uno spazio liberamente accessibile che permette una partecipazione diretta, ampia e diffusa. La Rete ci restituisce l'opinione della maggioranza oppressa dall'austerity. La Rete ci aiuta a uscire dalla Crisi. Si moltiplicanole imprese del cyber-attivismo, spauracchio delle agenzie di
intelligence governative come dei politici locali; le rivelazioni e gli attacchi continui al sistema di potere minano la già scarsa credibilità delle gerarchie stabilite. Si recita il mantra della partecipazione e dell'unità: Cittadini della Rete unitevi, la democrazia elettronica spazzerà via la corruzione.

Ecco gli <em>idola</em> che questo pamphlet si propone di abbattere, con argomenti filosofici e attraverso l'analisi delle tecnologie, della storia e della diffusione geo-politica della Rete. Con stile serrato e brillante, accessibile anche al lettore non specialista, gli autori mostrano che Rete aperta non significa Rete libera; pubblicare in Rete non significa rendere pubblico; decentramento e cooperazione non sono sinonimi immediati di democrazia. Non esistono soluzioni tecniche ai problemi politici; la democrazia non è un codice, né tanto meno un software. Non c'è un programma, né un programmatore capace di far funzionare meglio e risolvere i bug del sistema democratico.

<blockquote>
Ippolita è un gruppo di ricerca interdisciplinare attivo dal 2005. Conduce una riflessione a 360° sulle «tecnologie del dominio» e i loro effetti sociali. Pratica scritture conviviali in testi a circolazione trasversale, dal sottobosco delle comunità hacker alle aule universitarie. Tra i saggi pubblicati, <em>Open non è free. Comunità digitali tra etica hacker e mercato globale</em> (Elèuthera, Milano, 2005), <em>Luci e ombre di Google</em> (Feltrinelli, Milano, 2007, tradotto in francese, spagnolo e inglese), <em>Nell'acquario di Facebook</em> (Ledizioni, Milano, 2013, tradotto in francese, spagnolo e in corso di traduzione in inglese).
Ippolita tiene formazioni teorico-pratiche di autodifesa digitale, filosofia della tecnica, comunicazione politica e validazione delle fonti, per accademici, giornalisti, gruppi di affinità, persone curiose.
</blockquote>

[Ippolita.net](http://www.ippolita.net/)
