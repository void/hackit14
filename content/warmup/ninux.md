---
title: "Ninux"
where: "Sala Consiliare, via dello Scalo 21"
city: Bologna
date: 2014-03-28
hour: "20:30"
flyer: warmupninux.jpg
description: "Ninux.org è una Community wireless di persone che portano avanti un progetto alternativo di rete aperta, decentralizzata e di proprietà del cittadino."
passato: true
audio: "http://www.arkiwi.org/path64/aGFja21lZXRpbmcvSGFja2l0XzIwMTQvd2FybXVwL25pbnV4/html"
---

[Ninux.org](http://wiki.ninux.org/) è una Community wireless di persone che portano avanti un progetto alternativo di rete aperta, decentralizzata e di proprietà del cittadino.
