---
title: "NSA Confidential"
where: "Csa Next Emerson, via di Bellagio"
city: Firenze
date: 2014-03-22
flyer: nsaconfidential.jpg
passato: true
description: "Un lettura ragionata del Datagate, per provare a capire in che stato versa la rete e le strategie messe in campo per il controllo su vasta scala nell'epoca dei big data"
---
