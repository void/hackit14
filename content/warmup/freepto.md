---
title: "Presentazione di Freepto"
where: "Casa del Popolo, Corso Garibaldi, 117"
city: "Giulianova Paese"
date: 2014-03-29
flyer: sicurezzainrete.jpg
description: "Distribuzione GNU\Linux live (su penna USB) che permette di migliorare la sicurezza durante la navigazione e le attività online"
passato: true
---

Presentazione di [freepto](http://www.freepto.mx/)
