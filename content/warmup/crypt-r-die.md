---
title: "Presentazione di Freepto e Crypt'r'die"
where: "Csa Next Emerson, via di Bellagio"
city: Firenze
date: 2014-04-06
flyer: nsaconfidential.jpg
lat: 43.82164
lon: 11.22313
description: "Presentazione e laboratorio su [freepto](http://www.freepto.mx) e tematiche relative"
passato: true
---

Presentazione e laboratorio su [freepto](http://www.freepto.mx) e tematiche relative  
dalle 18:00

http://www.csaexemerson.it/dom-6-aprile-nsa-confidential-parte-seconda/
