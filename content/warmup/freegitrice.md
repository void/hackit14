---
title: "FREEgitrice"
where: "Casalecchio di Reno, via Canonica 18"
city: Bologna
date: 2014-05-12
hour: "20:30"
flyer: warmup_maggio_a5.jpg
description: "Giocare con RaspberryPI, freepto e un forno a microonde."
passato: true
---


Laboratorio finalizzato a creare un giocattolo che simulando il funzionamento di un forno a microonde (controllato da un RaspberryPI) permetta la creazione “plug&play” di chiavette [freepto](http://www.freepto.mx/).

Il caso d'uso: l'utente arriva con la sua pennina, inserisce la penna e preme il tasto di avvio. A scrittura completata il giocattolo emette il tipico suono di fine cottura.
