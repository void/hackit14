---
title: "Bitcoin Xposed"
where: "Biblioteca Roberto Ruffilli, Vicolo Bolognetti, 2"
city: Bologna
date: 2014-06-04
hour: "19:00"
flyer: warmup_giugno_web_small.jpg
description: "Approfondimenti e riflessioni sul fenomeno Bitcoin e monete elettroniche."
passato: true
---

Nonostante sia fatta di bit e non di atomi, Bitcoin è a tutti gli effetti una moneta reale, come le ricadute che produce: la sua diffusione è sempre maggiore e sono sempre di più gli esercizi commerciali, on-line ed off-line, che l'accettano come mezzo di scambio.

Nata nel 2009 dall'intuizione dell'enigmatico Satoshi Nakamoto, questa valuta digitale si differenzia da quelle tradizionali per tre motivi: è anonima, si basa sulla crittografia ed il suo circuito di transazioni è distribuito, non soggetto cioè alla regolamentazione di un unico ente centrale. Proprio a causa di queste sue caratteristiche essa viene spesso identificata come uno strumento intrinsecamente rivoluzionario, in grado di minare, se non addirittura di mettere fine, allo strapotere di banche centrali ed istituzioni finanziarie globali.

Ma è davvero così? Oppure presenta lati oscuri non ancora noti al grande pubblico? Qual'è la logica politica incorporata nel suo design? A quale modello economico allude? E sopratutto, è davvero possibile immaginare un circuito monetario, sebbene alternativo a quello tradizionale, come orizzonte di liberazione?

Ne parliamo Mercoledì 4 giugno alla Biblioteca Ruffilli con:

* Gabriele de Palma, giornalista, autore di Affare bitcoin. Pagare col P2P e senza le banche centrali.
* Andrea Fumagalli, economista
* Collettivo Autistici/Inventati
