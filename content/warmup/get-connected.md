---
title: "Get Connected"
where: "Labas Occupato, via Orfeo 46"
city: Bologna
date: 2014-05-22
hour: "20:00"
flyer: get_connected.jpg
description: "Ricostruire Internet: riflessioni, pratiche, strumenti per ripensare la rete oggi. A cura di eigenLab"
passato: true
audio: "http://www.arkiwi.org/path64/aGFja21lZXRpbmcvSGFja2l0XzIwMTQvd2FybXVwL2VpZ2VubGFi/html"
---

Ricostruire la rete Internet è ormai una parte essenziale per molti settori della nostra società.

Dall'amministrazione dei mercati, della finanza e dello Stato, alla gestione delle relazioni sociali, dell'informazione e della comunicazione, questa versatile infrastruttura manifesta ampiamente tutte le sue potenzialità.

Specialmente negli ultimi anni, con l'esplosione del web 2.0, il web dinamico dei social network e dei blog, internet ha assunto un ruolo rilevante nelle vite delle persone comuni, trasformando il paradigma economico capitalista dell'utente come passivo consumatore di servizi in utente come attivo partecipante della rete.

Ma come è stata creata la rete? Come si è evoluta e chi la gestisce adesso? Quali sono i problemi che risolve e quelli che invece pone? Come si possono sfruttare pienamente le potenzialità di Internetoggi? Quali sono le logiche e i limiti che ci vengono imposti? Esistono dei modi per agire all'interno della rete svincolati da queste coercizioni?

Tanti sono gli interrogativi aperti e altrettante le alternative da poter sperimentare.

In questo intervento cercheremo di disegnare un quadro generale della rete in quanto rete di calcolatori, infrastruttura, mezzo e spazio, guardando alle esperienze passate e a quelle attualmente presenti con l'ambizione di poter immaginare un futuro altro da quello imposto, un futuro non solo ideale ma che si possa anche costruire.

In particolare introdurremo le reti mesh comunitarie, analizzando in dettaglio l'esperienza di [eigenNet](https://eigenlab.org/eigennet/), una community nata a Pisa qualche anno fa.

