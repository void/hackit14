---
title: "Comunicazione e consapevolezza in rete"
where: "Laboratorio Gagarin, via Mario Capuani"
city: Teramo
date: 2014-03-28
flyer: sicurezzainrete.jpg
passato: true
description: "Come proteggere la propria privacy in rete e salvaguardare le proprie comunicazioni"
---
