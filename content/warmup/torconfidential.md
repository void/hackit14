---
title: "TOR: aromi e lacrime del routing a cipolla"
where: "Aula C4 Occupata"
city: Napoli
date: 2014-05-28
hour: "10:30"
flyer: "protormshack.png"
description: "TOR: aromi e lacrime del routing a cipolla"
passato: true
---

L’anonimato è un’esigenza imprescindibile per chi vuole preservare la propria privacy e vuole allo stesso tempo esprimere il proprio pensiero e le proprie idee liberamente. 

Lo scandalo del caso Datagate – con la rivelazione di tonnellate di materiale classificato top-secret ad opera dell’ex collaboratore della National Security Agency, Edward Snowden – ha accertato che è in atto un enorme programma di sorveglianza globale innescato dalle agenzie di Intelligence di vari Stati ai fini di controllo e di repressione e che la nostra privacy è svenduta al migliore offerente. Inoltre è noto che la maggior parte dei siti Web tengono traccia degli indirizzi IP dei loro visitatori, rendendo identificabili questi ultimi da chiunque abbia accesso a questi dati. Esistono tuttavia delle contromisure per eludere i sofisticati strumenti di chi ci controlla e ci censura, dandoci la possibilità di rimanere anonimi durante la navigazione su Internet. Uno degli strumenti più evoluti nell’ambito dell’anonimato è senza dubbio TOR (The Onion Router). Il progetto TOR consiste in una rete di server gestita dal lavoro di gruppi in difesa dei diritti digitali e da individualità di tutto il mondo che fa rimbalzare il traffico internet da una nazione all’altra prima di giungere a destinazione rendendo estremamente difficile l’identificazione di chi lo usa. Questo strumento è davvero a prova di NSA? È davvero sufficiente l’impiego di un solo software per essere anonimi in rete? Come hacklab abbiamo deciso di approfondire questa tematica e costruire un’iniziativa nella quale cercheremo di individuare il ruolo e il rapporto che TOR e le darknet hanno attualmente con Internet, descriveremo il funzionamento tecnico di tali software, tentando di individuarne debolezze e criticità al fine di sviluppare e consolidare una coscienza sull’utilizzo di tecnologie che supportano l’anonimato che, per essere efficaci, necessitano però di un utilizzo attento e corretto che parte dalla conoscenza del loro funzionamento. 

L’appuntamento è il 28 Maggio 2014 in [Aula C4 Occupata](http://c4occupata.org/). MSAck.
@ Complesso universitario Monte Sant'Angelo
