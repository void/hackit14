---
title: "Tuitpolitik"
where: "Hobo, giardini Filippo Re"
city: Bologna
date: 2014-02-01
hour: "20:00"
flyer: tuitpolitik.jpg
passato: true
description: "#tuitpolitik è uno spazio aperto di incontro (virtuale e reale) che si propone di ragionare collettivamente sul web (su twitter nello specifico) inteso come “campo di battaglia” politico."
---

*#tuitpolitik* e' uno spazio aperto di incontro (virtuale e reale) che si propone di ragionare 
collettivamente sul web (su twitter nello specifico) inteso come "campo di battaglia" politico.
