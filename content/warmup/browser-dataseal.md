---
title: "Browser Dataseal"
where: "Hobo, giardini Filippo Re"
city: Bologna
date: 2014-05-06
hour: "19:30"
flyer: warmup_maggio_a5.jpg
passato: true
description: "Seduta di autocoscienza tecnospirituale per rendere la navigazione on-line invisibile ai radar come un aereo malese."
slides: "browser_dataseal_hm14.odp"
audio: "http://www.arkiwi.org/path64/aGFja21lZXRpbmcvSGFja2l0XzIwMTQvd2FybXVwL2RhdGFzZWFs/html"
---

Seduta di autocoscienza tecnospirituale per rendere la navigazione invisibile ai radar come un aereo malese

####	 Big Brother is tracking you

Ok, come e quando, supponendo che il perche' lo conosciamo piu' o meno tutti

#### E io che ci posso fare?

Essere furtivi e mimetici in tre semplici click. Soddisfatti o rimborsati

#### Siamo tutti compromessi, qualcuno meno di altri

Non tutti i servizi sono uguali, perche' dovremmo sceglierne alcuni piuttosto che altri

#### Hands On

Porta il pc, lo renderemo inutilizzabile assieme!

