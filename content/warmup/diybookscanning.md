---
title: "DIY book scanning"
where: "Casalecchio di Reno, via Canonica 18"
city: Bologna
date: 2014-05-08
hour: "20:30"
flyer: diybookscanning.jpg
passato: true
description: "Buone pratiche e software libero per la digitalizzazione di testi cartacei e la pubblicazione nel web."
slides: "diybookscanning/"
audio: "http://www.arkiwi.org/path64/aGFja21lZXRpbmcvSGFja2l0XzIwMTQvd2FybXVwL2RpeWJvb2tzY2FubmluZw/html"
---


Buone pratiche e software libero per la digitalizzazione di testi cartacei e la pubblicazione nel web,
con dimostrazione d'uso del [diybookscanner](http://www.diybookscanner.org) e del software [spreads](http://github.com/diybookscanner/spreads).

Introduzione con presentazione dell'[Archivio Grafton9](http://grafton9.net), un repository di digitalizzazioni di libri, riviste e fanzine del circuito delle autoproduzioni dei centri sociali e delle controculture di fine anni 90.

