---
title: "4 Anni di MSAck"
where: "Mensa occupata"
city: Napoli
date: 2014-03-08
hour: "18:00"
flyer: "msack.png"
description: "Presentazione dell'hacklab, sintesi dei 4 anni di vita e presentazione del nuovo spazio MSAck in seno alla MensaOccupata e workshop: NSA, GCHQ, Snowden e Controllo Digitale"
passato: true
---

Sabato 8 Marzo e Lunedi' 10 Marzo, MSAck[0] festeggera' il suo compleanno con un doppio evento.

Sono passati 4 anni dalla nascita dell'hacklab MSAck: abbiamo trascorso anni difficili, di disorganizzazione, ma siamo sempre riusciti a essere uniti ed a mantenere una presenza costante ad hackmeeting.

Sicche' la scena e' non morta, forti di un percorso sul controllo e la repressione[1], MSAck ha sviluppato in questo ultimo anno due progetti in questa direzione, ma anche no.

SpyTheSpy: Fedeli al motto[1], abbiamo finalmente sviluppato per Android l'app "SpyTheSpy", con la quale e' possibile scattare una foto delle telecamere cittadine dal vostro scintillante ed iperpotente smartphone Android ed inviarla al nostro server autogestito, il quale prendera' in carico la foto geolocalizzata, aggiungendola automaticamente alla mappa cittadina, in modo da poter avere una mappatura piu' o meno precisa delle telecamere cittadine, baluardo del controllo e repressione. In questo modo ci proponiamo di eseguire una mappatura piu' o meno completa, in modo da poter sfuggire al controllo del Grande Fratello, in
nome della sicurezza.

L'app viene fornita in due modalita':

1. progetto Eclipse, in modo che tutti possono valutarne la genuinita', compilarlo ed installarlo
2. APK precompilato, da installare sul vostro smartphone (Ricordatevi di abilitare nelle impostazioni di sicurezza "Abilita origini sconosciute"/"Unknown sources")

Colibri'[2]: grazie al percorso che alcuni compagni dell'hacklab hanno svolto insieme ai collettivi studenteschi locali, e' emersa l'esigenza di progettare un software per la gestione delle biblioteche autogestite dagli spazi universitari occupati. E chi meglio di noi nerds poteva accogliere e soddisfare questa richiesta? E' nato, quindi, Colibri'.

In piu', quest'anno MSAck rilancia e raddoppia: oltre al 10 Marzo, si terra' un secondo evento, questa volta alla Mensa Occupata[3], in cui parleremo del cosidetto "Datagate", sintetizzando gli avvenimenti e
discutendo tutti insieme sulle implicazioni politiche e tecniche che tale situazione ha generato, provando a creare un percorso di lotta e resistenza comune non solo alla realta' nerd ma anche e soprattutto ai
compagni meno tech.

Inoltre, verra' presentato il nuovissimo spazio dell'hacklab (ancora in fase di costruzione, c'e' tanto lavoro da fare!) in seno alla Mensa, rifugio e laboratorio nerd di nerdaggini nerdose.

Entrambi gli eventi saranno accompagnati da pranzo/cena di autofinanziamento.
Ecco, dunque, il calendario delle attivita':

8 Marzo - Mensa Occupata

h. 18: Presentazione dell'hacklab, sintesi dei 4 anni di vita e presentazione del nuovo spazio MSAck in seno alla MensaOccupata: $nome_da_definire
A seguire, workshop: "NSA, GCHQ, Snowden e Controllo Digitale"
h. 21: Cena di autofinanziamento

10 Marzo - Aula C4 Occupata[4]
h. 10: Presentazione dell'hacklab, sintesi dei 4 anni di vita e presentazione dei progetti Colibri' e SpyTheSpy
h. 13: Pranzo di autofinanziamento

[0] http://www.msack.org

[1] "Non ho niente da nascondere...ma tu che tieni da guarda'?"

[2] http://colibri.msack.org

[2] http://www.mensaoccupata.org

[3] http://www.c4occupata.org

