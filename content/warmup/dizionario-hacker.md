---
title: "Un dizionario hacker"
where: "Vag61, via Paolo Fabbri 110"
city: Bologna
date: 2014-06-13
hour: "20:00"
flyer: dizionariohacker.jpg
description: "Presentazione dell'ultimo libro di Arturo di Corinto, un dizionario critico sul mondo della controcultura digitale."
passato: true
---

Presentazione del libro di Arturo di Corinto: [Un dizionario hacker](http://dicorinto.it/i-nemici-della-rete/libro-un-dizionario-hacker/)

Un dizionario ragionato dei termini più significativi della cultura hacker, in cui di ogni voce si dà definizione, interpretazione e storia. L’autore, con sguardo critico e tutt’altro che neutrale, sfata i pregiudizi e fa luce sul mondo della controcultura digitale, spesso percepito come illegale e pericoloso. Dalla A di Anonymous alla W di Wikileaks, passando per Bitcoin, Defacement, Free software, Gnu e Media activism, l’autore ci accompagna alla scoperta di uno dei movimenti più attivi nella lotta alla globalizzazione capitalista, nella tutela della democrazia partecipata, della condivisione del sapere e della libera circolazione della conoscenza. Lemma dopo lemma, emerge la prospettiva politica dei “pirati informatici” e il senso della loro battaglia fatta a suon di decrittazioni e remix. Perché “privato” è il participio passato di “privare”.
