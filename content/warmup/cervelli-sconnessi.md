---
title: "Cervelli sconnessi"
where: "Libreria Ubik, Via Irnerio 27"
city: Bologna
date: 2014-05-28
hour: "18:00"
flyer: cervellisconnessi.jpg
description: "Presentazione del libro con l'autore Giuliano Santoro, Wu Ming, Mazzetta e Marco Trotta"
passato: "true"
audio: "http://www.arkiwi.org/path64/aGFja21lZXRpbmcvSGFja2l0XzIwMTQvd2FybXVwL2NlcnZlbGxpX3Njb25uZXNzaQ/html"
---

Presentazione dell'ultimo libro di [Giuliano Santoro](http://suduepiedi.net/) "Cervelli sconnessi. La resistibile ascesa del net-liberismo e il
dilagare della stupidità digitale".  

Con l'[autore](https://twitter.com/amicoFaralla), [Wu Ming](https://twitter.com/Wu_Ming_Foundt), [Marco Trotta](https://twitter.com/mrta75) e [Mazzetta](https://twitter.com/mazzettam).

Diretta audio in streaming su www.wumingfoundation.com

Libreria [Ubik Irnerio](http://www.libreriairnerio.blogspot.it), via Irnerio 27, Bologna
