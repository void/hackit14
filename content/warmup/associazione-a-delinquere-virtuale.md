---
title: "Associazione a delinquere virtuale"
where: "VAG61 via Paolo Fabbri 110"
city: Bologna
date: 2014-06-20
hour: "20:00"
flyer: associazionedelinquerevirtuale.png
description: "La legislazione d'emergenza sul web italiano. Un dibattito con Carola Frediani
(giornalista), Fulvio Sarzana (avvocato), Marina Prosperi (avvocato)."
passato: true
---

*La legislazione d'emergenza sul web italiano*

Premessa: a maggio 2013 va in porto l'operazione tangodown del CNAIPIC contro Anonymous Italia.  
L'accusa formulata è pesante: associazione a delinquere virtuale. Le pene previste vanno dai 3 ai 7 anni. Per quella che è la configurazione del vincolo associativo questa fattispecie sembra un assurdo giuridico. Eppure, anche se in via non definitiva, la cassazione in due differenti casi (sempre inerenti Anonymous Italia) negli ultimi mesi l'ha confermato con due giudicati cautelari.  
In buona sostanza ha affermato: «L'associazione a delinquere è configurabile eccome».

Relatori

 * Carola Frediani, giornalista.
 * Fulvio Sarzana, avvocato.
 * Marina Prosperi, avvocato.


In ordine qualche articolo per saperne di più:

 1. [le prime discrepanze emerse su Tangodown](http://www.infoaut.org/index.php/blog/varie/item/7874-tangodown-lucrare-o-legiferare-sullemergenza)
 2. [intervista a Sarzana sull'associazione a delinquere virtuale](http://www.infoaut.org/index.php/blog/clipboard/item/8072-sono-legione-non-cyberterroristi)
 3. [Carola Frediani, analisi sulle carte dell'inchiesta](http://www.infoaut.org/index.php/blog/approfondimenti/item/9817-anonymous-tutti-i-retroscena-delloperazione-tango-down)
 4. [intervista a Marina Prosperi sul primo giudicato cautelare in merito all'associazione a delinquere virtuale](http://www.infoaut.org/index.php/blog/approfondimenti/item/9852-anonymous-associazione-a-delinquere-virtuale?-la-cassazione-dice-di-si)

