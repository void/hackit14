---
title: "Materiali"
type: page
notoc: true
---


### Warmup 
<div class="text-justify">Le slides e le registrazioni, quando disponibili, sono scaricabili dalle pagine dei singoli eventi.
Visita la sezione <a href="/warmup">Warmup</a></div>
<br/><br/>


### Logo e locandina
+ Logo Hackit0x11 [[download svg]](/static/uploads/materiali/logo_back_black_hackit0x11.svg) 
[[download png]](/static/uploads/materiali/logo_back_black_hackit0x11.png)
+ Locandina Hackit0x11 [[download jpg (~1MB)]](/static/uploads/materiali/hackit0x11-manifesto.jpg)
[[download jpg (~13MB)]](/static/uploads/materiali/hackit0x11-manifesto-highres.jpg)
<br/>

### Adesivi
+ Sticker HackIT0x11 versione 0 (nologo, cyber_rights) - [[download pdf]] (/static/uploads/materiali/sticker_version_00.pdf)
<br/>

### Banners
+ Sfondo chiaro
[[727x90px png]](/static/uploads/materiali/banners/hackit0x11_leaderboard_00.png)
[[125x125px png]](/static/uploads/materiali/banners/hackit0x11_square_00.png)
[[120x240px png]](/static/uploads/materiali/banners/hackit0x11_vertical_01.png)
[[120x240px senza citazione png]](/static/uploads/materiali/banners/hackit0x11_vertical_00.png)
+ Sfondo scuro 
[[88x31px png]](/static/uploads/materiali/banners/hackit0x11_micro_00.png)
[[727x90px png]](/static/uploads/materiali/banners/hackit0x11_leaderboard_01.png)
[[125x125px png]](/static/uploads/materiali/banners/hackit0x11_square_01.png)
[[120x240px png]](/static/uploads/materiali/banners/hackit0x11_vertical_02.png)
[[120x240px senza citazione png]](/static/uploads/materiali/banners/hackit0x11_vertical_03.png)
<br/>

### Posters
+ Manifesto del WarmUp di Maggio a Bologna - [[download jpg A3]](/static/uploads/materiali/warmup_maggio.jpg) - [[download A5 jpg]](/static/uploads/materiali/warmup_maggio_a5.jpg)
+ Manifesto del WarmUp di Giugno a Bologna - [[download jpg A3]](/static/uploads/materiali/warmup_giugno.jpg)
<br/>

### Comunicato stampa
+ [Comunicato](comunicato-stampa) di indizione della conferenza stampa per Hackmeeting 2014, il 24 giugno 2014 alle 12:00 presso *La Linea*.

### Spot radio 
+ Spot Hackmeeting 2014 - 
<a target="_blank" href="http://eustachio.indivia.net">Radio Eustachio</a> - 
<a target="_blank" href="http://www.arkiwi.org/path64/aGFja21lZXRpbmcvSGFja2l0XzIwMTQvc3BvdC9oYWNraXQxNF8wMS5vZ2c/html">[download da arkiwi]</a>
+ Spot warmup Browser Dataseal + DIYbookscanning  - 
<a target="_blank" href="http://eustachio.indivia.net">Radio Eustachio</a> - 
<a target="_blank" href="http://www.arkiwi.org/path64/aGFja21lZXRpbmcvSGFja2l0XzIwMTQvc3BvdC93YXJtdXBfZGF0YXNlYWxfZGl5YnMub2dn/html">[download da arkiwi]</a>
+ Spot warmup FREEgitrice + Cervelli Sconnessi - 
<a target="_blank" href="http://eustachio.indivia.net">Radio Eustachio</a> - 
<a target="_blank" href="http://www.arkiwi.org/path64/aGFja21lZXRpbmcvSGFja2l0XzIwMTQvc3BvdC93YXJtdXBfZnJlZWdpdHJpY2VfY2VydmVsbGlzY29ubmVzc2kub2dn/html">[download da arkiwi]</a>
<br/>

### Video
+ Spot Hackmeeting 2014 - 
<a target="_blank" href="http://www.smkvideofactory.com/">SMKvideofactory</a> - 
<a target="_blank" href="https://www.youtube.com/watch?v=UXbQc4J_ywI">[video su YouTube]</a>



