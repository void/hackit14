---
Title: "Corriere Bologna"
Date : 2014-06-25
press_type: web
press_home: "http://corrieredibologna.corriere.it"
press_link: "http://corrieredibologna.corriere.it/bologna/notizie/cronaca/2014/25-giugno-2014/dai-segreti-snowden-social-raduno-italiano-hacker-223459532934.shtml"
press_title: "Dai segreti di Snowden ai social: il raduno italiano degli hacker"
---
