---
Title: "Repubblica Bologna"
Date : 2014-06-29
press_type: web
press_home: "http://bologna.repubblica.it/"
press_link: "http://bologna.repubblica.it/cronaca/2014/06/29/news/un_giorno_da_hacker_ecco_come_costruire_un_drone_o_manipolare_la_rete-90298890/"
press_title: "Un giorno da hacker: come manipolare i social e costruire un drone con 200 euro"
---
