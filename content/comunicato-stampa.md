---
title: "Comunicato indizione conferenza stampa per Hackmeeting 2014"
type: page
notoc: true
url: materiali/comunicato-stampa
---

Dopo dodici anni torna a Bologna Hackmeeting, il raduno annuale italiano delle controculture digitali. Il **27, 28 e 29 giugno** schiere di hacker, coder, hacktivisti, cypherpunk e semplici appassionati si riuniranno presso lo **Spazio Pubblico Autogestito Xm24 in via Fioravanti 24** in una tre giorni dove condividere e sperimentare collettivamente dal basso progetti, idee e approcci critici alle tecnologie digitali.  
Sono più di quaranta i talk già in calendario e le tematiche affrontate saranno tra le più disparate: tecniche di autodifesa digitale e tutela della propria privacy; network e piattaforme collaborative per la condivisione e creazione di contenuti aperti e liberi; droni autocostruiti e macchine per lo *scanning* dei libri *DoItYourself*; racconti dalla prima linea della lotta alla sorveglianza globale ed esperienze applicate di *biohacking*.  
Il tutto in una cornice all’insegna dell’autogestione: ad Hackmeeting non ci sono organizzatori e fruitori ma solo partecipanti!
Conferenza stampa presso _La Linea_, Piazza Re Enzo 1/4, il _24 giugno 2014 alle ore 12:00_

Per ulteriori informazioni:  
**Sito web**: it.hackmeeting.org  
**E-mail**: hackit14@autistici.org

Back to [materiali](/materiali)