---
title: "Contatti"
type: page
notoc: true
---

Per contribuire/chiedere info o qualsiasi cosa puoi iscriverti alla lista di discussione [hackmeeting](https://www.autistici.org/mailman/listinfo/hackmeeting) o entrare nel canale IRC ``#hackit99`` su ``irc.autistici.org``.

L'assemblea organizzativa si riunisce ogni mercoledì a XM24, puoi contattarla scrivendo a [hackit14@autistici.org](mailto:hackit14@autistici.org)